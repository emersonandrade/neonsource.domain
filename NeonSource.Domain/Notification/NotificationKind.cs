﻿namespace NeonSource.Domain.Notifications
{
    public enum NotificationKind
    {
        Succes,
        Warning,
        Error
    }
}
