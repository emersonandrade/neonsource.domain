﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace NeonSource.Domain.Notifications
{
    public static class NotificationExtensions
    {
        private static readonly Dictionary<NotificationKind, Severity> SNotificadionKindMapper = new()
        {
            { NotificationKind.Error, Severity.Error },
            { NotificationKind.Warning, Severity.Warning },
            { NotificationKind.Succes, Severity.Info }
        };

        public static Severity ToSeverity(this NotificationKind notificationKind)
        {
            return SNotificadionKindMapper.TryGetValue(notificationKind, out var result) ?
                result :
                throw new ArgumentOutOfRangeException($"{nameof(NotificationKind)}: {notificationKind}");
        }

        public static ValidationFailure ToValidationFailure(this Notification notification)
        {
            return new ValidationFailure(
                notification.Code,
                notification.Message,
                notification.Kind.ToSeverity()
            );
        }
    }
}
