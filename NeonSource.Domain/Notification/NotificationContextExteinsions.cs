﻿using FluentValidation.Results;
using System.Linq;

namespace NeonSource.Domain.Notifications
{
    public static class NotificationContextExteinsions
    {
        public static void AddNotification(this INotificationContext context, ValidationResult validationResult)
        {
            var notifications = validationResult.Errors.Select(vr => vr.ToNotification());
            context.AddNotifications(notifications);
        }

        public static void AddNotification(this INotificationContext context, params Notification[] notification)
        {
            context.AddNotifications(notification);
        }

        public static bool IsInvalid(this INotificationContext context)
        {
            return context.Notifications.Any(n => n.Kind == NotificationKind.Error);
        }

        public static bool IsValid(this INotificationContext context)
        {
            return !context.IsInvalid();
        }
    }
}
