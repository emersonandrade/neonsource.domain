﻿using System.Collections.Generic;

namespace NeonSource.Domain.Notifications
{
    public interface INotificationContext
    {
        IEnumerable<Notification> Notifications { get; }

        void AddNotifications(IEnumerable<Notification> notifications);


    }
}
