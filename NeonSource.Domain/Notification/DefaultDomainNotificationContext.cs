﻿using System.Collections.Generic;

namespace NeonSource.Domain.Notifications
{
    public class DefaultDomainNotificationContext : INotificationContext
    {
        private readonly List<Notification> _notifications = new();

        IEnumerable<Notification> INotificationContext.Notifications => this._notifications;

        void INotificationContext.AddNotifications(IEnumerable<Notification> notifications)
        {
            this._notifications.AddRange(notifications);
        }
    }
}
