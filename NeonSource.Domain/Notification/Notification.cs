﻿namespace NeonSource.Domain.Notifications
{
    public record Notification(string Code, string Message, NotificationKind Kind);
}
