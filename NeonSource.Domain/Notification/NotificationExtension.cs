﻿using System.Text;

namespace NeonSource.Domain.Notifications
{
    public static class NotificationExtension
    {

        private static string FormatMessage(string message, object parameters)
        {
            var sbMessage = new StringBuilder(message);

            var props = parameters.GetType().GetProperties();

            foreach (var pi in props)
            {
                object value = pi.GetValue(parameters);

                sbMessage.Replace($"{{{pi.Name}}}", value?.ToString());
                sbMessage.Replace($"[{pi.Name}]", value?.ToString());
            }

            return sbMessage.ToString();
        }


        public static Notification Format(this Notification notification, object parameters)
        {
            var message = FormatMessage(notification.Message, parameters);
            return notification with { Message = message };
        }

        public static void Merge(this INotificationContext destiny, INotificationContext source)
        {
            destiny.AddNotifications(source.Notifications);
        }
    }
}
