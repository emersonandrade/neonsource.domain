﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace NeonSource.Domain.Notifications
{
    public static class FluentValidationExtensions
    {
        private static readonly Dictionary<Severity, NotificationKind> SSeverityMapper = new()
        {
            { Severity.Error, NotificationKind.Error },
            { Severity.Warning, NotificationKind.Warning },
            { Severity.Info, NotificationKind.Succes },
        };

        public static NotificationKind ToNotificationKind(this FluentValidation.Severity severity)
        {
            return SSeverityMapper.TryGetValue(severity, out var result) ?
                result :
                throw new ArgumentOutOfRangeException($"{nameof(Severity)}: {severity}");
        }

        public static Notification ToNotification(this ValidationFailure validationFailure)
        {
            return new Notification(
                validationFailure.ErrorCode,
                validationFailure.ErrorMessage,
                validationFailure.Severity.ToNotificationKind()
            );
        }
    }
}
