﻿using NeonSource.Domain.Notifications;
using System.Collections.Generic;

namespace NeonSource.Domain.Entities
{
    public class Entity<TId> : INotificationContext
    {
        public Entity(TId id)
        {
            Id = id;
        }

        public Entity()
        {

        }

        public TId Id { get; init; }
        private readonly List<Notification> _notifications = new();
        IEnumerable<Notification> INotificationContext.Notifications => this._notifications;
        void INotificationContext.AddNotifications(IEnumerable<Notification> notifications)
        {
            this._notifications.AddRange(notifications);
        }
    }
}
